# PEC 1 - Un juego de aventuras - Duelo en el Caribe!

## Descripción

El juego se desarrolla en algún lugar del Caribe, donde dos espadachines se disponen a batirse en duelo. El modo de juego está basado en el sistema de batallas del celebre juego Monkey Island. Las batallas se ganan lanzando insultos,  si el jugador que los recibe no devuelve una respuesta correcta al insulto. Nuestro rival en este juego será un Bot controlado por el juego.

## Cómo jugar

El jugador empieza la partida desde el menú principal haciendo click en el botón **Play**. Al principio del duelo, el juego lanza una moneda al aire y decide cuál de los dos rivales será el primero en atacar. Una vez decidido quién es el atacante y quién el defensor el juego nos muestra un menú desplegable con las posibles opciones. En el caso de que el jugador sea el atacante, se mostrará la lista de insultos posibles. En caso de que el jugador sea el defensor, el Bot lanzará un insulto y se mostrará la lista de posibles respuestas. Ambos jugadores tienen 3 vidas y pierden una con cada respuesta errónea al insulto del rival. Si la respuesta ha sido correcta, quien haya acertado obtendrá el turno de insultar. Al final del duelo, cuando uno de los dos contrincantes se haya quedado sin vidas, pasaremos al menú del final de la partida, desde el que podremos volver al menú principal haciendo click en **Back to Main Menu** o podemos jugar otra partida haciendo click en **Play again**. En ambos menús, tanto el principal como el de final de partida, tenemos la opción de **Exit** para terminar el juego. Podemos ver una partida de prueba en [este video](https://youtu.be/9iOE2iW2kxo).

## Estructura del proyecto

El proyecto está estructurado de la manera clásica de un proyecto sencillo de Unity. La carpeta **Assets** contiene todos los recursos del juego separados por tipos:

- **Animations:** contiene las animaciones de los personajes.
- **Audio:** contiene los audios del proyecto.
- **FontGradients:** contiene configuraciones de colores para los textos utilizadas por TextMesh Pro.
- **Prefabs:** contiene elementos del juego que queremos generar durante runtime.
- **Resources:** contiene un archivo json con todos los insultos y sus respuestas.
- **Scenes:** contiene las escenas del proyecto.
- **Scripts:** contiene los scripts del proyecto.
- **Sprites:** contiene los sprites del proyecto.
- **TextMesh Pro:** recurso importado para customizar los textos del juego.

## Clases principales

### MainMenuController y EndController

Controlan la lógica para navegar entre escenas utilizando los botones del menú principal y del de final de partida.

### GameController

Es la clase principal del juego y gestiona el flujo de la partida. Actúa como árbitro entre los dos contrincantes y se comunica con la UI cuando hay cambios en el estado de la partida que deben ser actualizados. Se rige por la siguiente máquina de estados:  
![alt text](https://gitlab.com/camilopalacios/pec-1---un-juego-de-aventuras/raw/master/img/game-controller-state-machine.jpg)

### Duelist

#### Duelist interface

Define cómo debe ser y qué métodos debe tener un Duelista.

#### Duelist implementación

Implementa la interfaz descrita anteriormente. Contiene la máquina de estados que rige el comportamiento del duelista en los diferentes estados de la partida.  
![alt text](https://gitlab.com/camilopalacios/pec-1---un-juego-de-aventuras/raw/master/img/duelist-state-machine.jpg)

####  PlayerDuelist y BotDuelist

Heredan de la clase anterior e implementan métodos distintos para Elegir insultos y respuestas. El jugador recibe estos inputs de la UI mientras que el Bot elige aleatoriamente de sus listas.

### InsultPod e InsultPods

Un InsultPod es una estructura formada por un insulto y su respuesta. InsultPods contiene una lista de todos los InsultPods creados a partir del archivo de recursos.

### InsultsManager

Carga el archivo de recursos json con la lista de insultos y respuestas y genera los InsultPods a partir de los datos extraídos del archivo. Sirve para pedir la lista de insultos, la de respuestas y para comprobar si hay match entre un insulto y una respuesta dadas.

### UI

#### UIController

Ofrece métodos para actualizar los diferentes campos de la UI durante la partida y de mostrar mensajes emergentes.

#### DialogScrollList

Genera una lista de botones de manera dinámica con los diálogos de insultos cuando el jugador ataca y de respuestas cuando el jugador defiende.

#### DuelResults

Esta clase es estática y contiene auto properties con datos que nos interesa pasar de una escena a otra. En este caso se usa para mostrar el mensaje de victoria o derrota en la escena de final del juego.

