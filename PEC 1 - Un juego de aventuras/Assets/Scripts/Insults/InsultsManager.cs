﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsultsManager
{
  private InsultPods insults;

  public InsultsManager(TextAsset insultsData)
  {
    insults = JsonUtility.FromJson<InsultPods>(insultsData.ToString());
  }

  public string[] getAllOffenses()
  {
    return insults.getAllOffenses();
  }

  public string[] getAllComebacks()
  {
    return insults.getAllComebacks();
  }

  public bool checkCorrectComeback(string offense, string comeback)
  {
    return insults.checkCorrectComeback(offense, comeback);
  }
}