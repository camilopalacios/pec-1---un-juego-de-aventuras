﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * An InsultPod consists of an offense and a comeback.
 */
[System.Serializable]
public class InsultPod
{
  public string offense;
  public string comeback;
  public InsultPod(string offense, string comeback){
    this.offense = offense;
    this.comeback = comeback;
  }
}

[System.Serializable]
public class InsultPods
{
  public InsultPod[] insults;

	public string[] getAllOffenses()
  {
    string[] offenses = new string[insults.Length];
    for (int i = 0; i < insults.Length; ++i)
    {
      offenses[i] = insults[i].offense;
    }
    return offenses;
  }

  public string[] getAllComebacks()
  {
    string[] comebacks = new string[insults.Length];
    for (int i = 0; i < insults.Length; ++i)
    {
      comebacks[i] = insults[i].comeback;
    }
    return comebacks;
  }

  public bool checkCorrectComeback(string offense, string comeback)
  {
    InsultPod result, tempPod = new InsultPod(offense, comeback);
    result = Array.Find(insults, pod => pod.offense == tempPod.offense && pod.comeback == tempPod.comeback);
    if (result != null) return true;
    return false;
  }
}
