﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum State
{
  idle,
  preparingAttack,
  awaitingComeback,
  preparingDefense,
  defending,
  hurt,
  roundWinner
};

public interface Duelist
{
  int Lives { get; set; }
  string Offense { get; set; }
  string Comeback { get; set; }

  bool IsTheAttacker();
  bool IsTheDefender();
  bool IsReadyToAttack();
  bool IsReadyToDefend();
  void GoToState(State state);
  void SetMissedTheAttack();
  void SetAsNextAttacker();
  void SetAsRoundWinner();
  void TakeDamage();
  void TakeAttack();
  void AcceptYield();
  void TakeOneLive();
  void LoadDialogs(string[] offenses, string[] comebacks);
  string[] GetAvailableOffenses();
  string[] GetAvailableComebacks();
  void SetNextOffense(string offense);
  void SetNextComeback(string comeback);
}
