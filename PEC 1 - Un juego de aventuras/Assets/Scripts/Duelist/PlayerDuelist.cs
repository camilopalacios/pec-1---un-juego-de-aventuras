﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDuelist : DuelistImpl
{
  public override string GiveOffense()
  {
    return Offense;
  }

  public override string GiveComeback()
  {
    return Comeback;
  }
}
