﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DuelistImpl : MonoBehaviour, Duelist
{
  private Animator animator;
  protected State currentState;
  private const int initialLives = 3;
  protected bool readyToAttack, readyToDefend;
  protected string[] offenses;
  protected string[] comebacks;
  public int Lives { get; set; }
  public string Offense { get; set; }
  public string Comeback { get; set; }
  public GameObject rival;
  public AudioController audioController;

  protected void Awake()
  {
    currentState = State.idle;
    Lives = initialLives;
    readyToAttack = false;
    readyToDefend = false;
    animator = GetComponent<Animator>();
  }

  void Update()
  {
    TriggerAudios();
    TriggerAnimations();
    switch (currentState)
    {
      case State.idle:
        resetAttackAndDefendFlags();
        break;
      case State.preparingAttack:
        Offense = GiveOffense();
        rival.GetComponent<Duelist>().TakeAttack();
        GoToState(State.awaitingComeback);
        break;
      case State.awaitingComeback:
        // Just waiting for the comeback
        break;
      case State.preparingDefense:
        Comeback = GiveComeback();
        GoToState(State.defending);
        break;
      case State.defending:
        // Just waiting for the offense
        break;
      case State.hurt:
        TakeOneLive();
        resetAttackAndDefendFlags();
        GoToState(State.preparingDefense);
        rival.GetComponent<Duelist>().AcceptYield();
        break;
      case State.roundWinner:
        resetAttackAndDefendFlags();
        GoToState(State.preparingAttack);
        break;
    }
  }

  public bool IsTheAttacker()
  {
    return currentState == State.preparingAttack || currentState == State.awaitingComeback;
  }

  public bool IsTheDefender()
  {
    return currentState == State.defending || currentState == State.awaitingComeback;
  }

  public bool IsReadyToAttack()
  {
    return readyToAttack;
  }

  public bool IsReadyToDefend()
  {
    return readyToDefend;
  }

  public void GoToState(State state)
  {
    currentState = state;
  }

  public void SetMissedTheAttack()
  {
    GoToState(State.idle);
  }

  public void SetAsNextAttacker()
  {
    GoToState(State.preparingAttack);
  }

  public void SetAsRoundWinner()
  {
    GoToState(State.roundWinner);
  }

  public void TakeDamage()
  {
    GoToState(State.hurt);
  }

  public void TakeAttack()
  {
    if (currentState == State.idle) GoToState(State.preparingDefense);
  }

  public void AcceptYield()
  {
    if (currentState == State.awaitingComeback) GoToState(State.preparingAttack);
  }

  public void TakeOneLive()
  {
    Lives--;
  }

  public void LoadDialogs(string[] offenses, string[] comebacks)
  {
    this.offenses = offenses;
    this.comebacks = comebacks;
  }

  public string[] GetAvailableOffenses()
  {
    return offenses;
  }

  public string[] GetAvailableComebacks()
  {
    return comebacks;
  }

  private void resetAttackAndDefendFlags()
  {
    readyToAttack = false;
    readyToDefend = false;
  }

  public void SetNextOffense(string offense)
  {
    Offense = offense;
    readyToAttack = true;
  }

  public void SetNextComeback(string comeback)
  {
    Comeback = comeback;
    readyToDefend = true;
  }

  private void TriggerAudios()
  {
    switch (currentState)
    {
      case State.hurt:
        audioController.PlayHit();
        break;
      case State.roundWinner:
        audioController.PlayDodge();
        break;
    }
  }

  private void TriggerAnimations()
  {
    switch (currentState)
    {
      case State.idle:
        animator.SetBool("Thrust", true); // Missed the attack
        break;
      case State.preparingAttack:
        ResetAnimatorBools();
        animator.SetBool("Attacking", true);
        break;
      case State.awaitingComeback:
        animator.SetBool("Attacking", true);
        break;
      case State.hurt:
        animator.SetBool("Hit", true);
        break;
      case State.defending:
        animator.SetBool("Attacking", false);
        break;
      case State.preparingDefense:
        ResetAnimatorBools();
        break;
      case State.roundWinner:
        animator.SetBool("Dodge", true);
        animator.SetBool("Thrust", true);
        break;
    }
  }

  private void ResetAnimatorBools(){
    animator.SetBool("Attacking", false);
    animator.SetBool("Dodge", false);
    animator.SetBool("Hit", false);
    animator.SetBool("Thrust", false);    
  }

  public abstract string GiveOffense();
  public abstract string GiveComeback();
}
