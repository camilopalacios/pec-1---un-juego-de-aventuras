﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotDuelist : DuelistImpl
{
  new void Awake()
  {
    base.Awake();
    FlipCharacter();
  }

  public override string GiveOffense()
  {
    readyToAttack = true;
    return GiveRandomOffense();
  }

  private string GiveRandomOffense()
  {
    int randomIndex = UnityEngine.Random.Range(0, offenses.Length);
    return offenses[randomIndex];
  }

  public override string GiveComeback()
  {
    readyToDefend = true;
    return GiveRandomComeback();
  }

  private string GiveRandomComeback()
  {
    int randomIndex = UnityEngine.Random.Range(0, comebacks.Length);
    return comebacks[randomIndex];
  }

  public void FlipCharacter()
  {
    Vector3 scale = transform.localScale;
    scale.x *= -1;
    transform.localScale = scale;
  }
}
