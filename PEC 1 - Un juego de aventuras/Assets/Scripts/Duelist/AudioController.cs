﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour {
	public AudioClip hit, dodge;
	public AudioSource audioSource;

	public void PlayHit(){
		PlaySoundEffect(hit);
	}

	public void PlayDodge(){
		PlaySoundEffect(dodge);
	}

	private void PlaySoundEffect(AudioClip clip){
		audioSource.clip = clip;
		audioSource.Play();
	}
}
