﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class EndController : MonoBehaviour {
	public TextMeshProUGUI endMessage;
  public TMP_ColorGradient green, red;
  public MusicController musicController;

	void Awake() {
		setEndMessage(DuelResults.EndMessage);
    setEndMessageColor(DuelResults.Winner);
	}

  private void setEndMessage(string message)
  {
    endMessage.text = message;
  }

  private void setEndMessageColor(bool winner){
    if(winner){
      endMessage.colorGradient = new VertexGradient(green.bottomLeft, green.bottomRight, green.topLeft, green.topRight);
      musicController.PlayVictory();
    }
    else {
      endMessage.colorGradient = new VertexGradient(red.bottomLeft, red.bottomRight, red.topLeft, red.topRight);
      musicController.PlayDefeat();
    }
  }

  public void PlayAgain ()
  {
		SceneManager.LoadScene("Game");
  }

  public void ToMainMenu ()
  {
		SceneManager.LoadScene("Menu");
  }

  public void ExitGame(){
    Debug.Log("Closing the application...");
    Application.Quit();
  }
}
