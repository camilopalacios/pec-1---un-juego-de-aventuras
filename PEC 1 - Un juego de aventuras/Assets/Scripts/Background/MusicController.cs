﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour {

	public AudioClip victory, defeat;
	public AudioSource audioSource;

	public void PlayVictory(){
		PlaySoundEffect(victory);
	}

	public void PlayDefeat(){
		PlaySoundEffect(defeat);
	}

	private void PlaySoundEffect(AudioClip clip){
		audioSource.clip = clip;
		audioSource.Play();
	}
}
