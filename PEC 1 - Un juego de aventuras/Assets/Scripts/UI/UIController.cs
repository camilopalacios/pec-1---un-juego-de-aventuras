﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIController : MonoBehaviour
{
  public TextMeshProUGUI playerStatus, botStatus, currentDialog, missionHint;
  public GameObject liveImage, toastMessage;

	void Awake() {
		playerStatus.text = "";
		botStatus.text = "";
		currentDialog.text = "";
		missionHint.text = "";

	}

  public void SetDialogs(string text)
  {
    currentDialog.text = text;
  }

	public void SetPlayerAttacking(){
		playerStatus.text = "Player: (atacando)";
		botStatus.text = "Bot: (defendiendo)";
	}

	public void SetBotAttacking(){
		playerStatus.text = "Player: (defendiendo)";
		botStatus.text = "Bot: (atacando)";
	}

  public void SetBotLives(int lives)
  {
		foreach (Transform child in GameObject.Find("BotLives").transform)
    {
      GameObject.Destroy(child.gameObject);
    }
    for (int i = 0; i < lives; ++i)
    {
      GameObject heart = (GameObject)GameObject.Instantiate(liveImage);
      heart.transform.SetParent(GameObject.Find("BotLives").transform);
      heart.transform.position = new Vector3 (
        heart.transform.parent.position.x + i*20,
        heart.transform.parent.position.y,
        heart.transform.parent.position.z
			);
    }
  }

  public void SetPlayerLives(int lives)
  {
    foreach (Transform child in GameObject.Find("PlayerLives").transform)
    {
      GameObject.Destroy(child.gameObject);
    }
    for (int i = 0; i < lives; ++i)
    {
      GameObject heart = (GameObject)GameObject.Instantiate(liveImage);
      heart.transform.SetParent(GameObject.Find("PlayerLives").transform);
      heart.transform.position = new Vector3 (
        heart.transform.parent.position.x + i*20,
        heart.transform.parent.position.y,
        heart.transform.parent.position.z
			);
    }
  }

	public void SetMissionHintAttack()
  {
		SetMissionHint("Elige un insulto de la lista:");
  }

	public void SetMissionHintDefend()
  {
		SetMissionHint("Elige una respuesta de la lista:");
  }

  public void SetMissionHint(string text)
  {
    missionHint.text = text;
  }

	public void ShowPlayerBeginToast(){
		ShowBeginToast("Tú comienzas!");
	}

	public void ShowBotBeginToast(){
		ShowBeginToast("Bot comienza!");
	}

	private void ShowBeginToast(string message){
		GameObject toast = (GameObject)GameObject.Instantiate(toastMessage);
		toast.transform.SetParent(GameObject.Find("UI").transform);
		toast.transform.position = toast.transform.parent.position;
		toast.GetComponentInChildren<TextMeshProUGUI>().text = message;
		Destroy(toast, 2f); // Toast message disappears after 2 secods
	}
}
