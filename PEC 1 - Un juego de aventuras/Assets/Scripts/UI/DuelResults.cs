﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DuelResults {
	public static string EndMessage { get; set;	}
	public static bool Winner { get; set; }
}
