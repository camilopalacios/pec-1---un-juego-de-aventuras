﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogsScrollList : MonoBehaviour
{
  public List<string> dialogList;
  public Transform contentPanel;
  public GameObject prefabButton;

  /**
	 * The dialogs we want to show in the scroll view. Offenses or Comebacks, depending on who is attacking.
	 */
  public void LoadDialogs(string[] dialogs)
  {
    dialogList.Clear();
    for (int i = 0; i < dialogs.Length; ++i)
    {
      dialogList.Add(dialogs[i]);
    }
    RefreshDisplay();
  }

  public void ClearDialogs(){
    ClearButtons();
  }

  private void RefreshDisplay()
  {
    ClearButtons();
    AddButtons();
  }

  private void ClearButtons()
  {
    foreach (Transform child in contentPanel)
    {
      GameObject.Destroy(child.gameObject);
    }
  }
  
  private void AddButtons()
  {
    for (int i = 0; i < dialogList.Count; ++i)
    {
      string dialog = dialogList[i];
      GameObject newButton = (GameObject)GameObject.Instantiate(prefabButton);
      newButton.transform.SetParent(contentPanel);
      newButton.GetComponentInChildren<Button>().onClick.AddListener(
        () => GameObject.Find("Player").GetComponent<Duelist>().SetNextOffense(newButton.GetComponentInChildren<Text>().text)
      );
      newButton.GetComponentInChildren<Button>().onClick.AddListener(
        () => GameObject.Find("Player").GetComponent<Duelist>().SetNextComeback(newButton.GetComponentInChildren<Text>().text)
      );
      DialogEntryButton dialogEntryButton = newButton.GetComponent<DialogEntryButton>();
      dialogEntryButton.setText(dialog);
    }
  }
}
