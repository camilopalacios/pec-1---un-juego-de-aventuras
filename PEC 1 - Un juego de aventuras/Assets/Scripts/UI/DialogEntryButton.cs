﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogEntryButton : MonoBehaviour
{
  public Button button;

	public void setText(string text){
		button.GetComponentInChildren<Text>().text = text;
	}
}
