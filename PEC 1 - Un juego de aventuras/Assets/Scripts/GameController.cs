﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
  private enum State
  {
    preparingRound,
    checkingOffenseReady,
    throwingOffense,
    checkingComebackReady,
    throwingComeback,
    decidingRoundWinner,
    checkingEndOfDuel
  }

  private enum ActiveDuelist
  {
    player,
    bot
  }

  private UIController ui;
  private State currentState;
  private InsultsManager insultsManager;
  private GameObject currentAttacker;
  private string currentOffense, currentComeback;
  private TextAsset insults;
  public GameObject player, bot;
  private Duelist botDuelist, playerDuelist;
  private float timingFlipCoin;

  // Use this for initialization
  void Start()
  {
    timingFlipCoin = Time.time + 1.4f; // The flip coin animation lasts 1.4 seconds
    currentState = State.preparingRound;
    // Load insults from the json file
    insults = Resources.Load("Insults") as TextAsset;
    insultsManager = new InsultsManager(insults);
    // Initialize the duelists
    InitializeDuelists(insultsManager.getAllOffenses(), insultsManager.getAllComebacks());
    // Get a reference to the UI controller
    ui = GameObject.Find("UI").GetComponent<UIController>();
  }

  // Update is called once per frame
  void Update()
  {
    if (Time.time < timingFlipCoin) return;
    if(GameObject.Find("Coin")) GameObject.Destroy(GameObject.Find("Coin"));

    switch (currentState)
    {
      case State.preparingRound:
        if (currentAttacker == null)
        {
          PickBeginner();
          RefreshLivesInUI();
        }
        currentState = State.checkingOffenseReady;
        break;
      case State.checkingOffenseReady:
        if (playerDuelist.IsTheAttacker() && playerDuelist.IsReadyToAttack()){
          currentState = State.throwingOffense;
        }
        else if(botDuelist.IsTheAttacker() && botDuelist.IsReadyToAttack())
        {
          // This is done so that the player can read the resulting dialog before going to the next round
          StartCoroutine(DelayAndGoToNextRound());
        }
        break;
      case State.throwingOffense:
        PrepareAttack();
        currentState = State.checkingComebackReady;
        break;
      case State.checkingComebackReady:
        if (
          playerDuelist.IsTheDefender() && playerDuelist.IsReadyToDefend()
          || botDuelist.IsTheDefender() && botDuelist.IsReadyToDefend()
        )
        {
          currentState = State.throwingComeback;
        }
        break;
      case State.throwingComeback:
        PrepareDefense();
        currentState = State.decidingRoundWinner;
        break;
      case State.decidingRoundWinner:
        CheckRoundWinner(currentAttacker, currentOffense, currentComeback);
        currentState = State.checkingEndOfDuel;
        break;
      case State.checkingEndOfDuel:
        RefreshLivesInUI();
        if(!IsEndOfDuel()) currentState = State.checkingOffenseReady;
        break;
    }
  }

  private void PickBeginner()
  {
    // Pick which player begins the first duel
    if (FlipCoin(0.5f))
    {
      ShowPlayerBeginToast();
      playerDuelist.SetAsNextAttacker();
      currentAttacker = player;
      LoadAttackDialogsInUI();
      RefreshRoundStatusInUI(ActiveDuelist.player);
      RefreshMissionHintInUI(ActiveDuelist.player);
    }
    else
    {
      ShowBotBeginToast();
      botDuelist.SetAsNextAttacker();
      currentAttacker = bot;
      LoadDefenseDialogsInUI();
      RefreshRoundStatusInUI(ActiveDuelist.bot);
      RefreshMissionHintInUI(ActiveDuelist.bot);
    }
  }

  private void PrepareAttack()
  {
    if (playerDuelist.IsTheAttacker())
    {
      currentOffense = playerDuelist.Offense;
      RefreshDialogInUI("Player: " + currentOffense);
    }
    else
    {
      currentOffense = botDuelist.Offense;
      RefreshDialogInUI("Bot: " + currentOffense);
    }
  }

  private void PrepareDefense()
  {
    if (playerDuelist.IsTheAttacker())
    {
      currentComeback = botDuelist.Comeback;
    }
    else
    {
      currentComeback = playerDuelist.Comeback;
    }
  }

  private void CheckRoundWinner(GameObject attacker, string offense, string comeback)
  {
    if (attacker.name == "Player")
    {
      RefreshDialogInUI("Player: " + offense + "\nBot: " + comeback);
      if (insultsManager.checkCorrectComeback(offense, comeback)) // BOT COMES BACK!!!
      {
        playerDuelist.SetMissedTheAttack();
        botDuelist.SetAsRoundWinner();
        currentAttacker = bot;
        LoadDefenseDialogsInUI();
        RefreshRoundStatusInUI(ActiveDuelist.bot);
        RefreshMissionHintInUI(ActiveDuelist.bot);
      }
      else
      {
        playerDuelist.SetAsRoundWinner();
        botDuelist.TakeDamage();
      }
    }
    else
    {
      RefreshDialogInUI("Bot: " + offense + "\nPlayer: " + comeback);
      if (insultsManager.checkCorrectComeback(offense, comeback))  // PLAYER COMES BACK!!!
      {
        botDuelist.SetMissedTheAttack();
        playerDuelist.SetAsRoundWinner();
        currentAttacker = player;
        LoadAttackDialogsInUI();
        RefreshRoundStatusInUI(ActiveDuelist.player);
        RefreshMissionHintInUI(ActiveDuelist.player);
        CleanDialogInUI();
      }
      else
      {
        botDuelist.SetAsRoundWinner();
        playerDuelist.TakeDamage();
      }
    }
  }

  private bool IsEndOfDuel()
  {
    if (playerDuelist.Lives <= 0 || botDuelist.Lives <= 0)
    {
      if (playerDuelist.Lives <= 0) {
        DuelResults.EndMessage = "Has sido derrotado... \nGAME OVER";
        DuelResults.Winner = false;
      }
      else {
        DuelResults.EndMessage = "¡Felicitaciones!\nEres el vencedor";
        DuelResults.Winner = true;
      }
      StartCoroutine(DelayEndOfGame());
      return true;
    }
    return false;
  }

  private void InitializeDuelists(string[] offenses, string[] comebacks)
  {
    botDuelist = bot.GetComponent<Duelist>();
    botDuelist.LoadDialogs(offenses, comebacks);
    playerDuelist = player.GetComponent<Duelist>();
    playerDuelist.LoadDialogs(offenses, comebacks);
  }

  /**
   * Returns true with a probability passed by parameter
   * probability: the probability of the return being true
   */
  private bool FlipCoin(float probability)
  {
    float randomNum = Random.Range(0f, 1f);
    return (randomNum <= probability);
  }

  private void LoadAttackDialogsInUI()
  {
    ui.GetComponentInChildren<DialogsScrollList>().LoadDialogs(playerDuelist.GetAvailableOffenses());
  }

  private void LoadDefenseDialogsInUI()
  {
    ui.GetComponentInChildren<DialogsScrollList>().LoadDialogs(playerDuelist.GetAvailableComebacks());
  }

  private void RefreshLivesInUI()
  {
    ui.SetPlayerLives(playerDuelist.Lives);
    ui.SetBotLives(botDuelist.Lives);
  }

  private void RefreshRoundStatusInUI(ActiveDuelist attacker)
  {
    switch (attacker)
    {
      case ActiveDuelist.player: ui.SetPlayerAttacking(); break;
      case ActiveDuelist.bot: ui.SetBotAttacking(); break;
    }
  }

  private void RefreshMissionHintInUI(ActiveDuelist attacker)
  {
    switch (attacker)
    {
      case ActiveDuelist.player: ui.SetMissionHintAttack(); break;
      case ActiveDuelist.bot: ui.SetMissionHintDefend(); break;
    }
  }

  private void RefreshDialogInUI(string text)
  {
    ui.SetDialogs(text);
  }

  private void CleanDialogInUI()
  {
    RefreshDialogInUI("");
  }

  private void ShowBotBeginToast()
  {
    ui.ShowBotBeginToast();
  }

  private void ShowPlayerBeginToast()
  {
    ui.ShowPlayerBeginToast();
  }

  // This delay is set on purpose to let the user read the dialogs of the final round (offense and comeback)
  IEnumerator DelayEndOfGame()
  {
    GameObject.Destroy(GameObject.Find("Scroll View"));
    GameObject.Destroy(GameObject.Find("Mission Hint Text"));
    yield return new WaitForSeconds(2f);
    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); // Load the next scene
  }

  // This delay is set on purpose to let the user read the dialogs of the final round (offense and comeback)
  IEnumerator DelayAndGoToNextRound()
  {
    yield return new WaitForSeconds(1f);
    currentState = State.throwingOffense;
  }
}
